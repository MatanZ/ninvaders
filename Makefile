CC=gcc
CFLAGS=-O3 -Wall
LIBS=-lncurses

CFILES=globals.c view.c aliens.c ufo.c player.c nInvaders.c
HFILES=globals.h view.h aliens.h ufo.h player.h nInvaders.h sounds.h
OFILES=globals.o view.o aliens.o ufo.o player.o nInvaders.o sounds.o
all:		nInvaders

nInvaders: 	$(OFILES) $(HFILES) $(CFILES)
		$(CC) $(LDFLAGS) -o$@ $(OFILES) $(LIBS)

sounds.c: sounds/makesounds
		cd sounds ; ./makesounds ; mv sounds.c ..

.c.o:
		$(CC) -c  -I. $(CFLAGS) $(OPTIONS) $<
clean:
		rm -f nInvaders $(OFILES) sounds.c
